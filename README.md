# pyUserManager

**What is this?**

pyUserManager is a MySQL database user management application writen in python.

It uses the MySQL.Connector library for database manipulation, and the PyQt5 library as a GUI.

PyUserManager is capable of registering, editing, and deleting users from said database. The database and table names, as well as the host, username and pswd for MySQL can be set using the variables set under "# MySQL data".

For a better experience you can pack the scirpt as a .exe file with pyinstaller.
A pre-packaged .exe is availiable on the repository as well, if you don't want to do it yourself.

By default the host is 'localhost', user 'root', pswd '', database is 'pumdb', and it's table 'pumtb'. This should work fine in most casual XAMPP environments.

**How does it work?**

When first started, PUM checks if the database exists. If it doesn't, it's gonna create it.
Then, it checks if a user with a ID of '0' exists inside the database. If not, it's going to prompt you to create one, after that, you're prompted with the login screen, where you can login with the admin user you just registered.

From there, you're greeted with the main interface, where a real-time list shows you the users currently sitting in the database, from there you can Register new users, Edit current ones like changing their name, password and roles, or delete them. Note that the user ID '0' you created at the start, cannot be deleted, or else it would just break everything, so you can only edit it. If user ID '0' gets deleted somehow, a fail-safe will prompt you to register a new one.

**What is it for?**

Made it to test my python skills, or lack of them. Experience makes you better. Also works as a portfolio.
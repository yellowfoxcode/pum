# PyUserManager by markprower
# Version 1.0.0

# Import libraries
import sys
import string
import bcrypt
import secrets
import mysql.connector
from PyQt5.QtWidgets import *
from mysql.connector import Error

# MySQL data
host = "localhost"
user = "root"
pswd = ""
db = "pumdb"
tb = "pumtb"

# Global vars
alphabet = string.ascii_letters + string.digits
loggedUsername = ''
selectedUser = ''
firstAdmin = False
isAdmin = False
isZero = False

# App start-up sequence
class startup(QWidget):
    def __init__(self):
        super().__init__()
        
        # Connect to host
        try:
            startup.conn = mysql.connector.connect(user=user, password=pswd, host=host)
        except Error as e:
            print(e)
            return
        
        # Set cursor
        startup.cursor = startup.conn.cursor()
        
        # Check if database exists
        startup.cursor.execute(f"SHOW DATABASES LIKE '{db}'")
        result = startup.cursor.fetchone()
      
        # If it does
        if result:
            print('Database found.')
            startup.conn.database = db
        
        # If it doesn't
        else:
            print('Database not found. Creating.')
            startup.cursor.execute(f"CREATE DATABASE {db}")
            startup.conn.database = db
            startup.cursor.execute(f"CREATE TABLE {tb} "
                           "(username VARCHAR(255), "
                           "password VARCHAR(255), "
                           "role VARCHAR(255), "
                           "unique_id VARCHAR(255))")
            
            print("A fresh database has been created.")
        
        # Check if admin 0 exists
        startup.cursor.execute(f"SELECT * FROM {tb} WHERE unique_id='0'")
        result = startup.cursor.fetchone()
      
        # If it does
        if result:
            print('Admin 0 found.')
            self.loginInterface = loginInterface()
            if not self.loginInterface.exec_():
                sys.exit()
        
        # If it doesn't
        else:
            print('Admin 0 not found. Creating.')
            global firstAdmin
            firstAdmin = True
            self.registerInterface = registerInterface()
            if not self.registerInterface.exec_():
                self.loginInterface = loginInterface()
                if not self.loginInterface.exec_():
                    sys.exit()
        
        # Set window properties
        self.setWindowTitle('PyUserManager by markprower')
        self.setFixedSize(800, 550)

# Register screen
class registerInterface(QDialog):
    def __init__(self):
        super().__init__()
        
        # Set window properties
        self.setWindowTitle('User registration')
        self.setFixedSize(250, 180)
        
        # GUI
        self.usernameLabel = QLabel('Username: ', self)
        self.usernameField = QLineEdit(self)
        
        self.passwordLabel = QLabel('Password: ', self)
        self.passwordField = QLineEdit(self)
        self.passwordField.setEchoMode(QLineEdit.Password)
        
        self.roleLabel = QLabel('Role: ', self)
        self.comboBox = QComboBox()
        self.comboBox.addItem("User")
        self.comboBox.addItem("Admin")
        
        # Lock role selection for admin 0
        if firstAdmin:
            self.comboBox.setCurrentIndex(1)
            self.comboBox.setEnabled(False)
        
        self.registerButton = QPushButton('Register User', self)
        self.registerButton.move(85, 95)
        
        userDataLayout = QVBoxLayout()
        userDataLayout.addWidget(self.usernameLabel)
        userDataLayout.addWidget(self.usernameField)
        userDataLayout.addWidget(self.passwordLabel)
        userDataLayout.addWidget(self.passwordField)
        userDataLayout.addWidget(self.roleLabel)
        userDataLayout.addWidget(self.comboBox)
        
        buttonLayout = QHBoxLayout()
        buttonLayout.addWidget(self.registerButton)
        
        mainLayout = QVBoxLayout()
        mainLayout.addLayout(userDataLayout)
        mainLayout.addLayout(buttonLayout)
        
        self.setLayout(mainLayout)
        
        self.registerButton.clicked.connect(self.register)
    
    # Registration validation
    def register(self):
        # Grab input values
        username = self.usernameField.text()
        password = self.passwordField.text()
        
        # Hash password
        password_hashed = bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt())
        
        # Get user role
        role = self.comboBox.currentText()
        
        # Check if it's the first admin
        global firstAdmin
        showZeroMsg = False
        
        # If it is
        if firstAdmin:
            unique_id = '0'
            showZeroMsg = True
            firstAdmin = False
        
        # If it isn't
        else:
            unique_id = ''.join(secrets.choice(alphabet) for i in range(8))
        
        # Insert
        # Check if user already exists
        startup.cursor.execute(f"SELECT * FROM {tb} WHERE username=%s", (username,))
        result = startup.cursor.fetchone()
        
        # If it does
        if result:
            QMessageBox.warning(self, 'Registration failed', 'User ' + username + ' already exists.')
        
        # If it doesn't
        else:
            add_data = (f"INSERT INTO {tb}"
                        "(username, password, role, unique_id)"
                        "VALUES (%s, %s, %s, %s)")
            data = (username, password_hashed, role, unique_id)
            startup.cursor.execute(add_data, data)
            startup.conn.commit()
            
            QMessageBox.warning(self, 'Registration successful', 'User ' + username + ' has been created.')

            if showZeroMsg:
                print('Admin 0 created.')
                showZeroMsg = False
        
        # Reset form
        self.usernameField.clear()
        self.passwordField.clear()
        
# Login screen
class loginInterface(QDialog):
    def __init__(self):
        super().__init__()
        
        # Set properties
        self.setWindowTitle('User login')
        self.setFixedSize(800, 550)
        
        # GUI
        self.usernameLabel = QLabel('Username: ', self)
        self.usernameLabel.move(315, 220)
        self.usernameField = QLineEdit(self)
        self.usernameField.move(370, 217)
        
        self.passwordLabel = QLabel('Password: ', self)
        self.passwordLabel.move(315, 250)
        self.passwordField = QLineEdit(self)
        self.passwordField.move(370, 247)
        self.passwordField.setEchoMode(QLineEdit.Password)
        
        self.loginButton = QPushButton('Login', self)
        self.loginButton.move(375, 280)
        self.loginButton.clicked.connect(self.checkCredentials)
        
    # Login validation
    def checkCredentials(self):
        # Grab input values
        username = self.usernameField.text()
        password = self.passwordField.text()
        
        # Check
        startup.cursor.execute(f"SELECT password, role FROM {tb} WHERE username=%s", (username,))
        result = startup.cursor.fetchone()
        
        if result:
            hashed_password = result[0].encode('utf-8')
            role = result[1]
            global isAdmin
            if bcrypt.checkpw(password.encode('utf-8'), hashed_password):
                self.userRole = role
                if 'Admin' in self.userRole:
                    isAdmin = True
                
                self.action = mainInterface()
                if not self.action.exec_():
                    sys.exit()
            else:
                QMessageBox.warning(self, 'Login failed', 'Invalid username or password.')
        else:
            QMessageBox.warning(self, 'Login failed', 'Invalid username or password.')

# Edit screen
class editInterface(QDialog):
    def __init__(self):
        super().__init__()
        
        # Set window properties
        self.setWindowTitle('User editing')
        self.setFixedSize(250, 230)
        
        # Get the current user's information from the database
        global selectedUser
        startup.cursor.execute(f"SELECT * FROM {tb} WHERE username=%s", (selectedUser,))
        self.userToUpdate = startup.cursor.fetchone()
        
        # GUI
        self.usernameLabel = QLabel('Current username: ' + self.userToUpdate[0])
        self.usernameEdit = QLineEdit()
        
        self.passwordCurrentLabel = QLabel('Current password: ', self)
        self.passwordCurrentField = QLineEdit()
        self.passwordCurrentField.setEchoMode(QLineEdit.Password)
        
        self.passwordNewLabel = QLabel('New password: ', self)
        self.passwordNewField = QLineEdit()
        self.passwordNewField.setEchoMode(QLineEdit.Password)
        
        self.roleLabel = QLabel('Current role: ' + self.userToUpdate[2])
        self.roleComboBox = QComboBox()
        self.roleComboBox.addItem("User")
        self.roleComboBox.addItem("Admin")
        
        if self.userToUpdate[3] == '0':
            self.roleComboBox.setEnabled(False)
        
        if self.userToUpdate[2] == 'Admin':
            self.roleComboBox.setCurrentIndex(1)
        
        self.updateButton = QPushButton('Update User', self)
        self.updateButton.move(85, 95)
        
        userDataLayout = QVBoxLayout()
        userDataLayout.addWidget(self.usernameLabel)
        userDataLayout.addWidget(self.usernameEdit)
        userDataLayout.addWidget(self.passwordCurrentLabel)
        userDataLayout.addWidget(self.passwordCurrentField)
        userDataLayout.addWidget(self.passwordNewLabel)
        userDataLayout.addWidget(self.passwordNewField)
        userDataLayout.addWidget(self.roleLabel)
        userDataLayout.addWidget(self.roleComboBox)
        
        buttonLayout = QHBoxLayout()
        buttonLayout.addWidget(self.updateButton)
        
        mainLayout = QVBoxLayout()
        mainLayout.addLayout(userDataLayout)
        mainLayout.addLayout(buttonLayout)
        
        self.setLayout(mainLayout)
        
        self.updateButton.clicked.connect(self.updateUser)
    
    # Logic
    def updateUser(self):
        # Build SQL query
        currentUsername = self.userToUpdate[0]
        query = f"UPDATE {tb} SET "
        data = []
        
        # Check if current password is correct
        currentPassword = self.passwordCurrentField.text()

        startup.cursor.execute(f"SELECT password, role FROM {tb} WHERE username=%s", (currentUsername,))
        result = startup.cursor.fetchone()
        
        if result:
            # If it is, proceed with edit
            dbPasswordHashed = result[0].encode('utf-8')
            if bcrypt.checkpw(currentPassword.encode('utf-8'), dbPasswordHashed):
                # Edit password
                if self.passwordNewField.text() != '':
                    query += "password=%s, "
                    newPassword = self.passwordNewField.text()
                    newPasswordHashed = bcrypt.hashpw(newPassword.encode('utf-8'), bcrypt.gensalt())
                    data.append(newPasswordHashed)
                
                # Edit username
                if self.usernameEdit.text() != '':
                    query += "username=%s, "
                    data.append(self.usernameEdit.text())
                    self.usernameLabel.setText('Current username: ' + self.usernameEdit.text())

                # Edit role
                if self.roleComboBox.currentText() != result[1]:
                    query += "role=%s, "
                    data.append(self.roleComboBox.currentText())
                    if self.roleComboBox.currentText() == 'Admin':
                        self.roleComboBox.setCurrentIndex(1)
                    else:
                        self.roleComboBox.setCurrentIndex(0)
                
                query = query[:-2] + " WHERE username=%s"
                data.append(self.userToUpdate[0])
                
                # Execute query
                startup.cursor.execute(query, data)
                startup.conn.commit()
                
                QMessageBox.warning(self, 'Info', 'User ' + currentUsername + ' data updated.')
            
            # If it isn't, display error
            else:
                QMessageBox.warning(self, 'Error', 'Old password does not match.')

        # Clear text fields
        self.usernameEdit.clear()
        self.passwordCurrentField.clear()
        self.passwordNewField.clear()

# Main app
class mainInterface(QDialog):
    def __init__(self):
        super().__init__()
        
        # Set window properties
        self.setWindowTitle('PyUserManager by markprower')
        self.setFixedSize(800, 550)
        
        # GUI
        self.searchLabel = QLabel('Search by username: ')
        self.searchBox = QLineEdit()
        
        self.userLabel = QLabel('User list: ')
        self.userList = QListWidget()
        self.userList.setSelectionMode(QListWidget.SingleSelection)
        
        self.editButton = QPushButton("Edit User")
        self.editButton.setEnabled(False)
        
        self.registerButton = QPushButton("Register User")
        self.registerButton.setEnabled(False)
        
        self.deleteButton = QPushButton("Delete User")
        self.deleteButton.setEnabled(False)
        
        searchLayout = QHBoxLayout()
        searchLayout.addWidget(self.searchLabel)
        searchLayout.addWidget(self.searchBox)
        
        buttonLayout = QHBoxLayout()
        buttonLayout.addStretch(1)
        buttonLayout.addWidget(self.editButton)
        buttonLayout.addWidget(self.registerButton)
        buttonLayout.addWidget(self.deleteButton)
        
        mainLayout = QVBoxLayout()
        mainLayout.addLayout(searchLayout)
        mainLayout.addWidget(self.userLabel)
        mainLayout.addWidget(self.userList)
        mainLayout.addLayout(buttonLayout)
        
        self.setLayout(mainLayout)
                
        self.searchBox.textChanged.connect(self.updateUserList)
        self.editButton.clicked.connect(self.callEditInterface)
        self.registerButton.clicked.connect(self.callRegisterInterface)
        self.deleteButton.clicked.connect(self.deleteUser)
        
        self.prePopulate()
        self.updateButtons()
        
    def testFunction(self):
        print("Test function executed!")
    
    # Pre-populate list with full table contents
    def prePopulate(self):
        startup.cursor.execute(f"SELECT username FROM {tb}")
        users = startup.cursor.fetchall()
        for user in users:
            self.userList.addItem(user[0])
    
    # Update user list by searching
    def updateUserList(self):
        query = self.searchBox.text()
        self.userList.clear()
        startup.cursor.execute(f"SELECT username FROM {tb} WHERE username LIKE '%{query}%'")
        users = startup.cursor.fetchall()
        for user in users:
            self.userList.addItem(user[0])
    
    # Allow buttons to work if user is a admin
    def updateButtons(self):
        global isAdmin
        if isAdmin:
            self.editButton.setEnabled(True)
            self.registerButton.setEnabled(True)
            self.deleteButton.setEnabled(True)
        else:
            self.editButton.setEnabled(False)
            self.registerButton.setEnabled(False)
            self.deleteButton.setEnabled(False)
    
    # Individual interface calls
    def callEditInterface(self):
        if self.userList.currentItem() is None:
            QMessageBox.warning(self, "Error", "No user selected from list.")
        else:
            global selectedUser
            selectedUser = self.userList.currentItem().text()
            self.action = editInterface()
            if not self.action.exec_():
                self.action = mainInterface
                self.userList.clear()
                startup.cursor.execute(f"SELECT username FROM {tb}")
                users = startup.cursor.fetchall()
                for user in users:
                    self.userList.addItem(user[0])
    def callRegisterInterface(self):
        self.action = registerInterface()
        if not self.action.exec_():
            self.action = mainInterface
            self.userList.clear()
            startup.cursor.execute(f"SELECT username FROM {tb}")
            users = startup.cursor.fetchall()
            for user in users:
                self.userList.addItem(user[0])
    
    # User deletion logic
    def deleteUser(self):
        if self.userList.currentItem() is None:
            QMessageBox.warning(self, "Error", "No user selected from list.")
        else:
            username = self.userList.currentItem().text()
            startup.cursor.execute(f"SELECT * FROM {tb} WHERE username=%s", (username,))
            result = startup.cursor.fetchone()
            idColumn = result[3]
            
            if idColumn == '0':
                QMessageBox.warning(self, 'Info', 'Admin ID 0 cannot be deleted.')
            else:
                query = f"DELETE FROM {tb} WHERE username=%s"
                startup.cursor.execute(query, (username,))
                startup.conn.commit()
                self.userList.takeItem(self.userList.currentRow())

# Run
if __name__ == '__main__':
    print('Welcome to PyUserManager | Version 1.0.0')
    app = QApplication(sys.argv)
    window = startup()
    window.show()
    startup.cursor.close()
    startup.conn.close()
    sys.exit(app.exec_())

# Script by markprower